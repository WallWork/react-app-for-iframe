
# Форма для интеграции в iframe партнёрами

## Содержание

- [Установка form-builder](#установка-form-builder)
- [Сборка](#сборка)

## Установка form-builder
- создать в корне файл `.npmrc`
- прописать
```
registry=https://nexus-new.tcsbank.ru/repository/npm-all/
```

## Сборка
### Сборка стилей
- в `css-loader` выставить опцию `modules: true` и `localIdentName: '[name]__[local]'`
- в `postcss-loader` добавить 2 плагина: `postcss-global-import` и `postcss-nested`

### Прод сборка
- если есть проблемы с минификацией - заменить использование webpack uglify плагина на сторонний `uglifyjs-webpack-plugin`
