import React, { Component } from 'react'
import MaskedInput from 'tinkoff-form-builder/lib/fields/MaskedInput/MaskedInput'
// import Button from 'tinkoff-form-builder/src/fields/Button/Button'

import styles from './InputWrapper.css'



export default WrappedComponent => {
    class InputWrapper extends Component {

        static displayName = `InputWrapper(${WrappedComponent.name})`;

        render() {
            return <div className={styles.InputFieldWrapper}>
                <WrappedComponent { ...this.props }>{ this.props.children }</WrappedComponent>
            </div>
        }
    }

    return InputWrapper;
};
