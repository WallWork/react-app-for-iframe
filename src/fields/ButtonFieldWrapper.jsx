import React, { PureComponent } from 'react'
// import Button from '@tinkoff-ui/button'
import Button from 'tinkoff-form-builder/lib/fields/Button/Button'

import { classnames } from '../utils'

import styles from './ButtonFieldWrapper.css'



class ButtonFieldWrapper extends PureComponent {
    render() {
        return <div className={classnames({
            [styles.ButtonFieldWrapper]: true,
            [styles['ButtonFieldWrapper_' + this.props.mod]]: !!this.props.mod,
        })}>
            <Button { ...this.props }>{ this.props.children }</Button>
        </div>
    }
}

export default ButtonFieldWrapper
