export const prodEnv = {
  configApi: 'https://config.tinkoff.ru/',
  dadataApi: 'https://api.tinkoff.ru/dadata/',
  tinkoffApi: 'https://api.tinkoff.ru/v1/',
  tinkoffInsuranceApi: 'https://api.tinkoffinsurance.ru/api/',
  buisnessApi: 'https://business.tinkoff.ru/api/v1/'

};

export const devEnv = {
  configApi: 'https://config-dev.tcsbank.ru/',
  dadataApi: 'https://www-qa4.tcsbank.ru/api/dadata/',
  tinkoffApi: 'https://www-qa4.tcsbank.ru/api/v1/',
  tinkoffInsuranceApi: 'https://api-test.tinkoffinsurance.ru:38002/api/',
  buisnessApi: 'https://business-qa2.tcsbank.ru/api/v1/'
};

export const Env = process.env.NODE_ENV === 'production' ? prodEnv : devEnv;
