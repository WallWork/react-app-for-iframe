import {formField} from 'tinkoff-form-builder';

// import Button from 'tinkoff-form-builder/lib/fields/Button/Button'
import ButtonFieldWrapper from './fields/ButtonFieldWrapper'
// import MaskedInput from 'tinkoff-form-builder/lib/fields/MaskedInput/MaskedInput'
import MaskedInput from 'tinkoff-form-builder/lib/fields/MaskedInput/MaskedInput'
import InputWrapper from './fields/InputWrapper'
import FioSuggest from 'tinkoff-form-builder/lib/fields/FioSuggest/FioSuggest'

import required from 'tinkoff-form-builder/lib/validators/required'
import regexp from 'tinkoff-form-builder/lib/validators/regexp'
import cyrillicOnly from 'tinkoff-form-builder/lib/validators/cyrillicOnly'
import fio from 'tinkoff-form-builder/lib/validators/fio'
import phone from 'tinkoff-form-builder/lib/validators/phone'


const VALIDATORS = {
    regexp,
    required,
    cyrillicOnly,
    fio,
    phone
};

// создаем словарь филдов, которые будем использовать в форме
const FIELDS = {
    fioSuggest: formField(InputWrapper(FioSuggest)),
    maskedInput: formField(InputWrapper(MaskedInput)),
    submit: formField(ButtonFieldWrapper)
};

const getSchema = ({ options = {}, p1, p2, p3 }) => {
console.log('{options, p1, p2, p3}', {options, p1, p2, p3}); // eslint-disable-line indent, no-console, object-curly-spacing
    return {
        fields: {
            name: {
                type: 'fioSuggest',
                props: {
                    placeholder: 'Иванов Алексей Петрович',
                    label: 'Имя, фамилия, отчество',
                    autoSwitchSymbols: true
                },
                validators: {
                    required: {
                        text: 'Поля обязателное'
                    },
                    cyrillicOnly: true,
                    fio: {
                        minWords: 2,
                        text: 'Не корректная информация. Введите фамилию, имя и отчество через пробел (Например: Иванов Иван Алексеевич)'
                    }
                }
            },
            phone_mobile: {
                type: 'maskedInput',
                value: '+7 (',
                props: {
                    label: 'Номер телефона',
                    placeholder: '    921) 123-45-67',
                    mask: '+7 (999) 999-99-99'
                },
                validators: {
                    phone: true,
                    regexp: {
                        rule: /^((8|\+7)[\- ]?)?(\(?[345689]\d{2}\)?)/ ,
                        text: 'Код города/оператора должен начинаться с цифры 3, 4, 5, 6, 8, 9'
                    }
                }
            },
            submit: {
                type: 'submit',
                props: {
                    mod: options.submit && options.submit.align,
                    text: 'Оформить карту'
                }
            }
        },
        structure: [
            [
                [
                    ['name'],
                    ['phone_mobile'],
                    ['submit']
                ],
                [],
                []
            ]
        ]
    }
};

export default {
    FIELDS,
    getSchema,
    VALIDATORS
}
