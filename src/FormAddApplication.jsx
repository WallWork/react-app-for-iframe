import React, {Component} from 'react';

import { BaseFormStore, Form } from 'tinkoff-form-builder';
import AddApplicationPlugin from 'tinkoff-form-builder/lib/plugins/AddApplication';
import { Env } from './configPresets';

import FormAddApplicationSchema from './FormAddApplicationSchema';
import { getJsonFromUrl, pick } from './utils';

import styles from './FormAddApplication.css';


const MESSAGE_OK = 'Спасибо за ваш запрос, мы вам перезвоним!';
const FORM_ALIGN = ['left', 'right', 'center'];
const AVALIABLE_PARAMS = [
    'app_type_id',
    'utm_source',
    'utm_medium',
    'tid',
    'channel',
    'track'
];

const getMetaInfo = (queryParams) => {
    return pick(AVALIABLE_PARAMS, queryParams);
};

class FormAddApplication extends Component {
    state = { submited: false };

    store = new BaseFormStore(Env);
    queryParams = getJsonFromUrl();
    schemaOptions = {};

    constructor(props) {
        super(props);

        const align = this.queryParams.align;

        this.schemaOptions = {
            submit: {
                align: FORM_ALIGN.indexOf(align) > -1 ? align : ''
            }
        };
    }

    checkAppId = ({ id }) => {
        if (id) {
            this.setState({ submited: true });

            window.parent.postMessage({ hid: id }, '*');
        }
    };

    initPlagin = () => {
        return new AddApplicationPlugin(() => ({
            completed: 1,
            app_type_id: 1,
            ...getMetaInfo(this.queryParams)
        }), {
            shouldSubmitOnFieldChange: false,
            onSubmitSuccess: this.checkAppId,
            requiredFields: ['name', 'phone_mobile']
        });
    };

    render() {
        const { getSchema, FIELDS, VALIDATORS } = FormAddApplicationSchema;

        return <div className={styles.root}>
            {this.state.submited ?
                <div className={styles.message}>
                    {MESSAGE_OK}
                </div>
                :
                <Form
                    storeInstance={this.store}
                    plugins={[this.initPlagin()]}
                    fieldsMap={FIELDS}
                    schema={getSchema({ options: this.schemaOptions })}
                    validators={VALIDATORS}
                />
            }
        </div>
    }
}

export default FormAddApplication;
