'use strict';

const getJsonFromUrl = () => {
    const query = window.location.search.substr(1);
    const parseParam = (result, value) => {
        const item = value.split("=");
        return {
            ...result,
            [item[0]]: decodeURIComponent(item[1])
        }
    };

    const params = query.split('&');
    return params != '' ? params.reduce(parseParam, {}) : {};
};

const pick = (params, data) => {
    return params.reduce((result, item) => ({
         ...result,
         [item]: data[item]
    }), {});
};

const classnames = function() {
	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg)) {
				classes.push(classNames.apply(null, arg));
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

    return classNames;
}();


export {
    getJsonFromUrl,
    pick,
    classnames
}
